#!/bin/sh
CMD="$1"

if [ "$(id -u)" -ne 0 ]; then
  printf "%s: must be superuser.\n" "$( basename "$0")" 2>&1
exit 1
fi
if [ -z "$1" ]; then
  printf "usage: %s [symlink|update|mkinfo|compile] <program> <version>\n" "$(basename "$0")"
  exit 1
fi

cd /Programs/infodir || exit

mkinfo() {
printf "bin\nsbin\nusr/bin\nusr/sbin\n" > BINDIR
printf "usr/share/man/man1\n" > DOCDIR
printf "lib\nusr/lib\nusr/lib64\n" > LIBDIR
}

checkprogram() {
if [ -z "$1" ]; then
  printf "Please give a program name\n"
  exit 1
fi
if [ -z "$2" ]; then
  printf "Please give a program version\n"
  exit 1
fi
}

symlink() {
TARGET="$1/$2"
grep -v '^ *#' /Programs/infodir/BINDIR \
               /Programs/infodir/LIBDIR \
               /Programs/infodir/DOCDIR \
               | cut -d : -f 2 \
               | while IFS= read -r dir
do
  for bin in /Programs/"$TARGET"/"$dir"/*; do
    if [ -f "$bin" ]; then
      ln -sv "$bin" "/$dir/$(basename "$bin")";
    fi
  done
done
}

update() {
TARGET="$1/$2"

cd /Programs/"$TARGET"/src || exit
if [ -d ".git" ]; then
  git pull -v
fi
}

compile() {
TARGET="$1/$2"

cd /Programs/"$TARGET"/src || exit
if [ -f /Programs/"$TARGET"/src/configure ]; then
  ./configure --prefix=/Programs/"$TARGET"/usr
fi

make
make install PREFIX=/Programs/"$TARGET"/usr MANPREFIX='${PREFIX}'/share/man
}

case "$CMD" in
sym*)
  checkprogram "$2" "$3"
  symlink "$2" "$3"
  ;;
upd*)
  checkprogram "$2" "$3"
  update "$2" "$3"
  ;;
com*)
  checkprogram "$2" "$3"
  compile "$2" "$3"
  ;;
mki*)
  mkinfo
  ;;
*)
  printf "usage: %s [symlink|update|mkinfo|compile] <program> <version>\n" "$(basename "$0")"
  exit 1
  ;;
esac
